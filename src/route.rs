use super::pages::*;
use yew::prelude::*;
use yew_router::prelude::*;

#[derive(Clone, Routable, PartialEq)]
pub enum Route {
    #[at("/")]
    Root,

    #[at("/about")]
    About,

    #[not_found]
    #[at("/404")]
    NotFound,
}

impl Route {
    pub fn render(self) -> Html {
        match self {
            Route::Root => html! { <Root />},
            Route::About => html! { <About /> },
            Route::NotFound => html! { <NotFound /> },
        }
    }
}
