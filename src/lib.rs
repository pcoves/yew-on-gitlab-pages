mod layout;
mod pages;
mod route;

use self::layout::Layout;
use yew::prelude::*;
use yew_router::prelude::*;

#[function_component]
pub fn App() -> Html {
    html! {
        <HashRouter>
            <Layout />
        </HashRouter>
    }
}
