use yew_on_gitlab_pages::App;

fn main() {
    yew::Renderer::<App>::new().render();
}
