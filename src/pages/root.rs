use yew::prelude::*;

#[function_component]
pub fn Root() -> Html {
    html! {
        <Hero />
    }
}

#[function_component]
fn Hero() -> Html {
    html! {
        <section class={classes!("hero", "is-primary", "is-fullheight-with-navbar")}>
            <div class={classes!("hero-body", "is-justify-content-center")}>
                <div>
                    <p class={classes!("title")}>
                        {"Yew on Gitlab pages"}
                    </p>
                    <p class={classes!("subtitle")}>
                        {"For free CSR hosting"}
                    </p>
                </div>
            </div>
        </section>
    }
}
