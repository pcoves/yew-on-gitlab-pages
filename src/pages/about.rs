use yew::prelude::*;

#[function_component]
pub fn About() -> Html {
    html! {
        <section class={classes!("content")}>
            <div class={classes!("container")}>
                <h1 class={classes!("title")}>{"Lorem Ipsum"}</h1>

                <p>{"Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tincidunt interdum justo, id eleifend magna convallis ut. Nunc tempor ipsum augue, in tempus turpis porttitor eget. Nulla euismod sapien turpis, at dapibus felis egestas ut. Nunc ac mi ipsum. Vivamus quis mauris eu magna pulvinar auctor vel nec lorem. Etiam faucibus arcu tincidunt sapien accumsan, non bibendum est viverra. Nulla id congue ante."}
                </p>

                <p>{"Donec sagittis feugiat euismod. Quisque in orci libero. Sed ultrices a ante sed efficitur. Nulla feugiat, lectus sit amet sagittis imperdiet, dui nunc hendrerit nunc, ac ultricies sapien orci eu enim. Fusce sagittis, odio eu consectetur gravida, risus nisl suscipit nibh, nec bibendum mi ante vel augue. Aliquam erat volutpat. Suspendisse ut dolor et turpis commodo porta sed non mi."}</p>

                <p>{"Fusce consequat tellus ac purus malesuada vestibulum. Fusce pulvinar mi eu dui euismod lacinia. Praesent congue nisl enim, in convallis nisl vehicula semper. Curabitur eget quam lorem. Vivamus scelerisque nisl et est hendrerit tincidunt. Nunc vehicula tristique mattis. Pellentesque facilisis elementum nisi nec congue. Morbi blandit risus tincidunt, dapibus sem ut, fringilla velit."}</p>

                <p>{"Pellentesque sed fermentum felis, sit amet laoreet urna. Sed sagittis lorem erat, et commodo tellus eleifend sit amet. Sed fringilla ultricies malesuada. Curabitur semper vestibulum vehicula. Donec eu lobortis ipsum. Nullam commodo augue mauris, at convallis ante ornare sed. Nullam tempus nulla nec felis elementum porttitor. Nunc congue nisi ac sapien hendrerit tristique."}</p>

                <p>{"Praesent suscipit nulla at arcu condimentum pellentesque. Mauris pulvinar, leo non consequat tincidunt, diam metus tempus nisi, sit amet aliquam nibh arcu at diam. Nunc dictum velit id tellus venenatis vehicula. Curabitur id velit et est pharetra egestas non et purus. Fusce accumsan ante non mi molestie, ac imperdiet nisi pulvinar. Mauris purus magna, ultricies quis malesuada et, molestie eget magna. Donec vel eros varius, convallis augue non, euismod mauris"}</p>
            </div>
        </section>
    }
}
