use yew::prelude::*;

#[function_component]
pub fn Footer() -> Html {
    html! {
        <footer class={classes!("footer")}>
            <div class={classes!("container")}>
                <ul class={classes!("columns", "has-text-centered")}>
                    <li class={classes!("column")}><a href="https://yew.rs/" target="_blank" rel="noopener noreferrer">{ "Yew" }</a></li>
                    <li class={classes!("column")}><a href="https://docs.rs/yew-router/latest/yew_router/" target="_blank" rel="noopener noreferrer">{ "Router" }</a></li>
                    <li class={classes!("column")}><a href="https://trunkrs.dev/" target="_blank" rel="noopener noreferrer">{ "Trunk" }</a></li>
                    <li class={classes!("column")}><a href="https://bulma.io/" target="_blank" rel="noopener noreferrer">{ "Bulma" }</a></li>
                </ul>
            </div>
        </footer>
    }
}
