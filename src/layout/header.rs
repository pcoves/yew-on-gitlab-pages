use super::nav::Nav;
use yew::prelude::*;

#[function_component]
pub fn Header() -> Html {
    html! {
        <header>
            <Nav />
        </header>
    }
}
