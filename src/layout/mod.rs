mod footer;
mod header;
mod nav;

use self::{footer::Footer, header::Header};
use super::route::Route;
use yew::prelude::*;
use yew_router::prelude::*;

#[function_component]
pub fn Layout() -> Html {
    html! {
        <>
            <Header />
            <main>
                <Switch<Route> render={Route::render} />
            </main>
            <Footer />
        </>
    }
}
