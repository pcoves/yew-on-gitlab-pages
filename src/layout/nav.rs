use crate::route::Route;
use yew::prelude::*;
use yew_router::prelude::*;

#[function_component]
pub fn Nav() -> Html {
    let is_active = use_state(bool::default);

    let button = use_node_ref();
    let menu = use_node_ref();

    let onclick = {
        let is_active = is_active.clone();
        Callback::from(move |_| is_active.set(*is_active ^ true))
    };

    {
        let button = button.clone();
        let menu = menu.clone();

        use_effect_with_deps(
            move |is_active| {
                button
                    .cast::<web_sys::HtmlButtonElement>()
                    .unwrap()
                    .set_class_name(if **is_active {
                        "navbar-burger is-active"
                    } else {
                        "navbar-burger"
                    });

                menu.cast::<web_sys::HtmlDivElement>()
                    .unwrap()
                    .set_class_name(if **is_active {
                        "navbar-menu is-active"
                    } else {
                        "navbar-menu"
                    });
            },
            is_active,
        )
    }

    html! {
        <nav class={classes!("navbar", "is-fixed-top")} role="navigation" aria-label="main navigation">
            <div class={classes!("container")}>
                <div class={classes!("navbar-brand")}>
                    <Link<Route> to={Route::Root} classes={classes!("navbar-item")}>
                        <img src="https://yew.rs/img/logo.svg" alt="Yew's logo" />
                    </Link<Route>>

                    <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbar-menu" ref={button} onclick={onclick.clone()}>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                    </a>
                </div>

                <div id="navbar-menu" class={classes!("navbar-menu")} ref={menu} {onclick}>
                    <Link<Route> to={Route::Root} classes={classes!("navbar-item")}>{"Home"}</Link<Route>>
                    <Link<Route> to={Route::About} classes={classes!("navbar-item")}>{"About"}</Link<Route>>

                    <div class={classes!("navbar-end")}>
                        <div class={classes!("navbar-item")}>
                            <div class={classes!("buttons")}>
                                <a href=r#"https://gitlab.com/pcoves/yew-on-gitlab-pages"# target="_blank" rel="noopener noreferrer" class={classes!("button", "is-link")}>{"Source code"}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    }
}
